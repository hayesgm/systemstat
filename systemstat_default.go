// Copyright (c) 2013 Phillip Bond
// Licensed under the MIT License
// see file LICENSE

// +build !linux

package systemstat

func getUptime(procfile string) (uptime UptimeSample) {
  panic("getUptime not implemented for non-linux systems")
}

func getLoadAvgSample(procfile string) (samp LoadAvgSample) {
  panic("getLoadAvgSample not implemented for non-linux systems")
}

func getMemSample(procfile string) (samp MemSample) {
  panic("getMemSample not implemented for non-linux systems")
}

func getProcCPUSample() (s ProcCPUSample) {
  panic("getProcCPUSample not implemented for non-linux systems")
}

func getCPUSample(procfile string) (samp CPUSample) {
  panic("getCPUSample not implemented for non-linux systems")
}

func getSimpleCPUAverage(first CPUSample, second CPUSample) (avg SimpleCPUAverage) {
  panic("getSimpleCPUAverage not implemented for non-linux systems")
}

func subtractAndConvertTicks(first uint64, second uint64) float64 {
  panic("subtractAndConvertTicks not implemented for non-linux systems")
}

func getCPUAverage(first CPUSample, second CPUSample) (avg CPUAverage) {
  panic("getCPUAverage not implemented for non-linux systems")
}

func getProcCPUAverage(first ProcCPUSample, second ProcCPUSample, procUptime float64) (avg ProcCPUAverage) {
  panic("getProcCPUAverage not implemented for non-linux systems")
}

func parseCPUFields(fields []string, stat *CPUSample) {
  panic("parseCPUFields not implemented for non-linux systems")
}
